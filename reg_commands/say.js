export default {
  name: 'say',
  description: 'Repeats what you say',
  execute: (msg, args) => {
    const text = args.join(' ');
    msg.channel.createMessage(text);
  },
};
