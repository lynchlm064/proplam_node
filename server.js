// server.js
import express from 'express';
import { fileURLToPath } from 'url';
import path from 'path';
import { bot, eris  } from './index.js';
import clc from "cli-color"
import dotenv from "dotenv";

dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();
const port = process.env.PROPLAM_PORT;

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'index.html'));
});

// Start the bot
app.get('/start', (req, res) => {
  if (!bot.ready) {
    bot.connect();
    eris.connect();
  }
  console.log(
    "================\n" +
    "bot " + clc.green("connected\n") +
    "================")  

  res.send('Bot started.');
});

// Stop the bot
app.get('/stop', (req, res) => {
  if (bot.ready) {
    bot.disconnect({ reconnect: false });
    eris.disconnect({ reconnect: false });
  }
  console.log(
    "================\n" +
    "bot " + clc.red("disconnected\n") +
    "================")
  res.send('Bot stopped.');
});

// Stats for the bot
app.get('/stats', (req, res) => {
  // Replace with your logic to fetch bot statistics
  const botStats = {
    guilds: eris.guilds.size, // Number of guilds the bot is in
    users: eris.users.size, // Number of users the bot can see
    channels: eris.channelGuildMap.size, // Number of channels across all guilds
  };
  res.json(botStats);
});

//start
app.listen(port, () => {
  console.log(`Express server is running on ` + clc.blue(`http://localhost:`) + clc.red(`${port}`));
});