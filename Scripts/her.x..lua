
 --[[
 
    ██╗  ██╗███████╗██████╗ ███╗██╗  ██╗███╗
    ██║  ██║██╔════╝██╔══██╗██╔╝╚██╗██╔╝╚██║
    ███████║█████╗  ██████╔╝██║  ╚███╔╝  ██║
    ██╔══██║██╔══╝  ██╔══██╗██║  ██╔██╗  ██║
    ██║  ██║███████╗██║  ██║███╗██╔╝ ██╗███║
    ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚══╝╚═╝  ╚═╝╚══╝

    [https://luaobfuscator.com]
 ]]--
  
local v0 = string.char
local v1 = string.byte
local v2 = string.sub
local v3 = bit32 or bit
local v4 = v3.bxor
local v5 = table.concat
local v6 = table.insert
local function v7(v8, v9)
    local v10 = {}
    for v33 = 1, #v8 do
        v6(v10, v0(v4(v1(v2(v8, v33, v33 + 1)), v1(v2(v9, 1 + (v33 % #v9), 1 + (v33 % #v9) + 1))) % 256))
    end
    return v5(v10)
end
function Patch(v11, v12)
    local v13 = v7("\221\202\217\44\234\233\196\14\193\141\200\42", "\126\177\163\187\69\134\219\167")
    local v14 = ""
    if (tabl0001 == nil) then
        tabl0001 = {}
    end
    local v15 = 658 - (161 + 497)
    local v16 = v12:gsub(v7("\102\222\97", "\156\67\173\74\165"), "")
    if ((v16:len() % (203 - (43 + 158))) ~= (0 - 0)) then
        return print(
            v7(
                "\23\191\76\21\183\102\95\59\162\91\86\180\35\94\116\182\78\23\181\40\8\116\132\70\27\185\50\78\61\185\78\86\181\53\6\35\165\70\24\187\102\82\60\178\91\19\242",
                "\38\84\215\41\118\220\70"
            )
        )
    end
    local v17 = v16:len() / 2
    for v34, v35 in ipairs(gg.getRangesList(v13)) do
        if (v35.type:sub(3 + 0, 775 - (201 + 571)) == "x") then
            v15 = v35.start + v11
            break
        end
    end
    local v18 = {}
    local v19 = {}
    for v36 = 1139 - (116 + 1022), v17 do
        v18[v36] = {
            [v7("\81\18\38\0\251\67\5", "\158\48\118\66\114")] = v15 + (v36 - (4 - 3)),
            [v7("\173\40\17\49\96", "\155\203\68\112\86\19\197")] = gg.TYPE_BYTE
        }
    end
    gg.loadResults(v18)
    local v20 = gg.getResults(gg.getResultsCount())
    for v38 in ipairs(v20) do
        local v39 = 0 + 0
        local v40
        while true do
            if (v39 == 0) then
                v40 = 0 - 0
                while true do
                    if (v40 == 0) then
                        v14 = string.format(v7("\3\197", "\152\38\189\86\156\32\24\133"), v20[v38].value)
                        v14 = string.upper(v14)
                        v40 = 3 - 2
                    end
                    if (v40 == (861 - (814 + 45))) then
                        if (v14:len() == (2 - 1)) then
                            v14 = "0" .. v14
                        end
                        v19[v38] = v14
                        break
                    end
                    if (v40 == (1 + 0)) then
                        v14 =
                            v14:gsub(v7("\218\113\129\96\218\113\129\96\218\113\129\96\218\113", "\38\156\55\199"), "")
                        if (v14 == "0") then
                            v14 = v14:gsub("0", v7("\248\45", "\35\200\29\28\72\115\20\154"))
                        end
                        v40 = 1 + 1
                    end
                end
                break
            end
        end
    end
    v14 = table.concat(v19)
    v14 = "h" .. v14
    local v21 = #tabl0001 + (886 - (261 + 624))
    local v22 = #tabl0001 + (3 - 1)
    local v23 = #tabl0001 + (1083 - (1020 + 60))
    tabl0001[v21] = v13
    tabl0001[v22] = v11
    tabl0001[v23] = v14
    gg.loadResults(v18)
    gg.getResults(v17)
    gg.editAll("h" .. v12, gg.TYPE_BYTE)
    gg.clearResults()
end
function Restore(v27)
    local v28 = 0
    local v29
    local v30
    local v31
    while true do
        if (v28 == 0) then
            v29 = 0
            v30 = nil
            v28 = 1
        end
        if (v28 == 1) then
            v31 = nil
            while true do
                if ((1423 - (630 + 793)) == v29) then
                    for v42 = 1, #tabl0001 do
                        if ((tabl0001[v42] == lib) and (tabl0001[v42 + 1] == v27)) then
                            local v49 = 0
                            while true do
                                if (v49 == (0 - 0)) then
                                    edi = tabl0001[v42 + 2]
                                    hex = #tabl0001[v42 + (9 - 7)] - 1
                                    break
                                end
                            end
                        end
                    end
                    for v43, v44 in ipairs(gg.getRangesList()) do
                        if (v44.type:sub(2 + 1, 9 - 6) == "x") then
                            targetAddr = v44.start + v27
                            break
                        end
                    end
                    v29 = 1
                end
                if ((1749 - (760 + 987)) == v29) then
                    hex = hex / (1915 - (1789 + 124))
                    for v45 = 767 - (745 + 21), hex do
                        v30[v45] = {
                            [v7("\24\187\213\205\136\63\39", "\84\121\223\177\191\237\76")] = targetAddr +
                                (v45 - (1 + 0)),
                            [v7("\189\90\200\167\41", "\161\219\54\169\192\90\48\80")] = gg.TYPE_BYTE
                        }
                    end
                    v29 = 7 - 4
                end
                if (v29 == (11 - 8)) then
                    gg.loadResults(v30)
                    gg.getResults(gg.getResultsCount())
                    v29 = 1 + 3
                end
                if (1 == v29) then
                    v30 = {}
                    v31 = {}
                    v29 = 2 + 0
                end
                if (v29 == 4) then
                    gg.editAll(edi, 1056 - (87 + 968))
                    gg.clearResults()
                    break
                end
            end
            break
        end
    end
end
off = v7("\114\77\6\35\116", "\69\41\34\96")
on = v7("\135\204\217\55", "\75\220\163\183\106\98")
co = off
function yyy()
    local v32 = 0 - 0
    while true do
        if ((1 + 0) == v32) then
            if ((cost == (4 - 2)) or nil) then
                os.exit()
            end
            break
        end
        if (v32 == (1413 - (447 + 966))) then
            cost =
                gg.choice(
                {co .. v7("\18\187\159\52\209", "\185\98\218\235\87"), v7("\201\61\36\237", "\202\171\92\71\134\190")},
                nil
            )
            if (cost == 1) then
                if (co == off) then
                    Patch(offset, v7("\33\196\52", "\232\73\161\76"))
                    co = on
                else
                    local v47 = 0
                    local v48
                    while true do
                        if (v47 == (701 - (376 + 325))) then
                            v48 = 0
                            while true do
                                if (v48 == (0 - 0)) then
                                    Restore(offset)
                                    co = off
                                    break
                                end
                            end
                            break
                        end
                    end
                end
                yyy()
            end
            v32 = 1
        end
    end
end
while true do
    yyy()
end
