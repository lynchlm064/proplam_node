export default {
    name: 'ping',
    description: 'Pong!',
    execute: (i) => {
       /* await*/ i.createMessage('Pong!');
    },
};