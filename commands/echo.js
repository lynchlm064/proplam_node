const yourUserId = '690666917328977941'; // Replace with your Discord user ID


export default {
    name: 'echo',
    description: 'Echo a message',
    options: [
      {
        name: 'message',
        description: 'The message to echo',
        type: 3, // 3 corresponds to 'STRING' for a string input
        required: true,
      },
    ],
   execute: async (interaction) => {
    // Check if the interaction user is you (replace 'YOUR_USER_ID' with your Discord user ID)
      const message = interaction.options.getString('message');
      await interaction.reply({ content: `You said: ${message}`, ephemeral: true });
  },
};