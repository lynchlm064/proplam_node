//const fs = require('fs');
const tempFileName = 'beautified.lua';
import fs from "fs";

export default {
  name: 'beautifylua',
  description: 'Beautify a Lua file',
  options: [
    {
      name: 'file',
      description: 'The Lua file to beautify',
      type: 3,
      required: true,
    },
  ],
  execute: async (interaction) => {
    const file = interaction.options.getString('file');

    if (file.endsWith('.lua')) {
      // Save the beautified code to a temporary file
      const tempFileName = 'beautified.lua';
      fs.writeFileSync(tempFileName, beautifiedCode);

      // Send the beautified code as a reply
      await interaction.reply({
        content: 'Here is the beautified Lua code:',
        files: [tempFileName],
      });

      // Clean up the temporary file
      fs.unlinkSync(tempFileName);
    } else {
      await interaction.reply('Please provide a Lua file with a .lua extension.');
    }
  },
};
