//const { convertJsToTs, convertJsToTsSync } = require( 'js-to-ts-converter' );
import { convertJsToTs, convertJsToTsSync } from  "js-to-ts-converter"

// Async
convertJsToTs( '/workspace/proplam_node' ).then( 
    () => console.log( 'Done!' ),
    ( err ) => console.log( 'Error: ', err )
); 


// Sync
//convertJsToTsSync( './index2.js' );
//console.log( 'Done!' );