local clickers = gg.choice({
              "⬅️️ • Back",
              "💰 • Task clicker",
              "🏆 • Pass reward clicker",
              "🏷 • Gallery sets clicker",
              "🔰 • Black market clicker"}
        ,nil,"☝️ • clickers")
--exits--
if clickers == nil then
os.exit()
    end 
if clickers == 1 then
os.exit()
      end

--task clicker--
if clickers == 2 then
gg.clearResults()
gg.setRanges(gg.REGION_CODE_APP)
gg.searchNumber("h F3 7B BF A9 F3 03 00 AA E7 FF FF 97 E0 00 00 36 60 2A 40 F9 60 01 00 B4 08 00 40 F9 09 85 59 A9 20 01 3F D6 80 00 00 36 E0 03 1F 2A F3 7B C1 A8 C0 03 5F D6 E0 03 13 AA F3 7B C1 A8 C0 FF FF 17 57 48 52 97 FE 0F 1F F8 00 28 40 F9 A0 00 00 B4 08 00 40 F9 02 85 59 A9 FE 07 41 F8 40 00 1F D6 4F 48 52 97", gg.TYPE_QWORD)
gg.getResults(8)
gg.editAll("h00008052C0035FD6", gg.TYPE_QWORD)
gg.clearResults()
     end

--pass reward clicker--
if clickers == 3 then
gg.clearResults()
gg.setRanges(gg.REGION_CODE_APP)
gg.searchNumber("h F5 53 BE A9 F3 7B 01 A9 95 78 02 90 A8 4E 7A 39 F3 03 01 2A F4 03 00 AA C8 00 00 37 20 51 02 F0 00 F4 47 F9 C4 CC C7 97 28 00 80 52 A8 4E 3A 39 94 3A 40 F9 E0 03 13 2A E1 03 1F AA 1E A3 5B 94 34 01 00 B4 28 51 02 F0 08 F5 47 F9 F3 7B 41 A9 E1 03 00 AA E0 03 14 AA 02 01 40 F9 F5 53 C2 A8 1C A0 F6 17", gg.TYPE_BYTE)
gg.getResults(32)
gg.editAll("h 00008052C0035FD6", gg.TYPE_BYTE)
gg.clearResults()
     end
     
--gallery clicker--
if clickers == 4 then
gg.clearResults()
gg.setRanges(gg.REGION_CODE_APP)
gg.searchNumber("h FF 83 03 D1 F4 63 00 F9 F3 7B 0D A9 14 8D 02 F0 88 BE 7E 39 F3 03 00 AA C8 00 00 37 C0 6A 02 D0 00 AC 45 F9 6C 1E D2 97 28 00 80 52 88 BE 3E 39 93 05 00 B4 D4 6A 02 D0 94 AE 45 F9 80 02 40 F9 08 E0 40 B9 48 00 00 35 9F 1E D2 97 E0 03 13 AA ED 00 00 94 60 04 00 37 E0 03 1F AA 6B 59 2F 94 60 82 C2 3C", gg. TYPE_BYTE) 
gg.getResults(8)
gg.editAll("h 20 00 80 D2 C0 03 5F D6", gg.TYPE_BYTE)
gg.clearResults() 
gg.setRanges(gg.REGION_CODE_APP)
gg.searchNumber("h F6 0F 1D F8 F5 53 01 A9 F3 7B 02 A9 15 8D 02 D0 96 67 02 90 A8 F6 7E 39 D6 EE 42 F9 F3 03 01 2A F4 03 00 AA 28 01 00 37 C0 6A 02 B0 00 AC 45 F9 1D 1A D2 97 80 67 02 90 00 EC 42 F9 1A 1A D2 97 28 00 80 52 A8 F6 3E 39 C0 02 40 F9 08 E0 40 B9 48 00 00 35 50 1A D2 97 E0 03 1F AA CE 02 C7 94 60 02 00 B4", gg. TYPE_BYTE) 
gg.getResults(8)
gg.editAll("h 00 00 80 52 C0 03 5F D6", gg.TYPE_BYTE) 
gg.clearResults() 
    end
   
--black market clicker--
if clickers == 5 then
gg.clearResults()
gg.setRanges(gg.REGION_CODE_APP)
gg.searchNumber("h F3 7B BF A9 F3 03 00 AA C9 E6 FF 97 80 00 00 B4 61 A2 40 B9 F3 7B C1 A8 7F F5 FF 17 5D CB 21 97 F3 7B BF A9 F3 03 00 AA C1 E6 FF 97 00 01 00 B4 61 A2 40 B9 A6 F3 FF 97 A0 00 00 B4 00 90 00 91 E1 03 1F AA F3 7B C1 A8 56 A0 B5 17 51 CB 21 97 FF 43 01 D1 FE 23 00 F9 00 E4 00 6F E8 03 00 91 E0 03 01 AD", gg.TYPE_BYTE)
gg.getResults(32)
gg.editAll("h 00008052C0035FD6", gg.TYPE_BYTE)
gg.clearResults()
    end            