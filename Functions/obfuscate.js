import axios from "axios"; //require("axios");
import { sleep } from "./Webhook_&_sleep.js";
import { beautifyLua } from "./Beutify.js";

export async function obfuscator(code) {
  const response = await axios.post("https://luaobfuscator.com/api/ide/init?f=3", code);
  await new Promise((resolve) => setTimeout(resolve, 1000));
  const sessionId = response.data.sessionId;
  const obfuscator = axios.create({
    headers: {
      sessionId: sessionId
    }
  });
  const obfuscateResponse = await obfuscator.post("https://luaobfuscator.com/api/ide/obfuscateAll/pre-2/2");
  const tokens = obfuscateResponse.data.tokens;
  let proplam = "";
  for (let i = 0; i < tokens.length; i++) {
    proplam += tokens[i].value;
  }
       const herx = `
 --[[
 
    ██╗  ██╗███████╗██████╗ ███╗██╗  ██╗███╗
    ██║  ██║██╔════╝██╔══██╗██╔╝╚██╗██╔╝╚██║
    ███████║█████╗  ██████╔╝██║  ╚███╔╝  ██║
    ██╔══██║██╔══╝  ██╔══██╗██║  ██╔██╗  ██║
    ██║  ██║███████╗██║  ██║███╗██╔╝ ██╗███║
    ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚══╝╚═╝  ╚═╝╚══╝

    [https://luaobfuscator.com]
 ]]--
  `;
 const prop = proplam.split("\n")[11];
 const afprop = beautifyLua(prop);
 const ret =  herx + "\n" + afprop;
 return ret;
}
