import {formatText} from 'lua-fmt';

export function beautifyLua(code) {
 return formatText(code);
}