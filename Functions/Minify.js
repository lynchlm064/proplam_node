import luamin from 'luamin';
export function minifyLua(luaCode) {
 return luamin.minify(luaCode, {
  RenameVariables: true,
  RenameGlobals: true,
  SolveMath: true,
 });
 
//  return luaCode.replace(/\n/g, ';').replace(/\s+/g, ' ').replace(/['"]/g, '\\$&').replace(/;;/g, ";");
};

