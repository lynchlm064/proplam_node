// function name is _
let _ = (n, d) => {
 return d == 0 ? 0 : n / d;
};

// function name is countDivisors
let countDivisors = (n) => {
 let count = 0;
 for (let i = 1; i <= n; i++) {
    if (n % i == 0) {
      count++;
    }
 }
 return count;
};

// function name is getNextDivisor
let getNextDivisor = (n, divisor) => {
 for (let i = divisor + 1; i <= n; i++) {
    if (n % i == 0) {
      return i;
    }
 }
 return -1;
};

// function name is printDivisors
let printDivisors = (n) => {
 console.log(`Divisors of ${n}:`);
 for (let i = 1; i <= n; i++) {
    if (n % i == 0) {
      console.log(i);
    }
 }
};

// function name is sumDigits
let sumDigits = (n) => {
 let sum = 0;
 while (n > 0) {
    sum += n % 10;
    n = Math.floor(n / 10);
 }
 return sum;
};

// function name is gcd
let gcd = (a, b) => {
 while (b != 0) {
    let temp = b;
    b = a % b;
    a = temp;
 }
 return a;
};

// function name is isPalindrome
let isPalindrome = (s) => {
 let start = 0;
 let end = s.length - 1;
 while (start < end) {
    if (s[start] != s[end]) {
      return false;
    }
    start++;
    end--;
 }
 return true;
};

// function name is stringReverse
let stringReverse = (s) => {
 let reversed = '';
 for (let i = s.length - 1; i >= 0; i--) {
    reversed += s[i];
 }
 return reversed;
};

// function name is rotateString
let rotateString = (s, d) => {
 if (s.length == 0) {
    return s;
 }
 let rotation = d % s.length;
 let first = s.substring(0, rotation);
 let second = s.substring(rotation);
 return second + first;
};

// function name is replaceString
let replaceString = (s, find, replace) => {
 return s.split(find).join(replace);
};

// function name is toBase
let toBase = (n, base) => {
 let result = '';
 let digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
 while (n > 0) {
    let remainder = n % base;
    n = Math.floor(n / base);
    result = digits[remainder] + result;
 }
 return result;
};

// function name is factorial
let factorial = (n) => {
 let fact = 1;
 for (let i = 1; i <= n; i++) {
    fact *= i;
 }
 return fact;
};

// function name is fibonacci
let fibonacci = (n) => {
 if (n <= 1) {
    return n;
 }
 let fib = [0, 1];
 for (let i = 2; i <= n; i++) {
    fib[i] = fib[i - 1] + fib[i - 2];
 }
 return fib[n];
};

// function name is prime
let prime = (n) => {
 if (n <= 1) {
    return false;
 }
 for (let i = 2; i <= Math.sqrt(n); i++) {
    if (n % i == 0) {
      return false;
    }
 }
 return true;
};


// function name is mergeSort
let mergeSort = (arr) => {
 if (arr.length < 2) {
    return arr;
 }
 let mid = Math.floor(arr.length / 2);
 let left = arr.slice(0, mid);
 let right = arr.slice(mid);
 return merge(mergeSort(left), mergeSort(right));
};

let merge = (left, right) => {
 let result = [];
 let i = 0;
 let j = 0;
 while (i < left.length && j < right.length) {
    if (left[i] < right[j]) {
      result.push(left[i]);
      i++;
    } else {
      result.push(right[j]);
      j++;
    }
 }
 while (i < left.length) {
    result.push(left[i]);
    i++;
 }
 while (j < right.length) {
    result.push(right[j]);
    j++;
 }
 return result;
};


// function name is binarySearch
let binarySearch = (arr, target) => {
 let left = 0;
 let right = arr.length - 1;
 while (left <= right) {
    let mid = Math.floor((left + right) / 2);
    if (arr[mid] == target) {
      return mid;
    }
    if (arr[mid] < target) {
      left = mid + 1;
    } else {
      right = mid - 1;
    }
 }
 return -1;
};

// function name is binarySearchFirstOccurrence
let binarySearchFirstOccurrence = (arr, target) => {
 if (arr.length == 0) {
    return -1;
 }
 let left = 0;
 let right = arr.length - 1;
 while (left <= right) {
    let mid = Math.floor((left + right) / 2);
    if (arr[mid] < target) {
      left = mid + 1;
    } else if (arr[mid] > target) {
      right = mid - 1;
    } else {
      if (mid == 0 || arr[mid - 1] != target) {
        return mid;
      } else {
        right = mid - 1;
      }
    }
 }
 return -1;
};

// function name is binarySearchLastOccurrence
let binarySearchLastOccurrence = (arr, target) => {
 if (arr.length == 0) {
    return -1;
 }
 let left = 0;
 let right = arr.length - 1;
 while (left <= right) {
    let mid = Math.floor((left + right) / 2);
    if (arr[mid] < target) {
      left = mid + 1;
    } else if (arr[mid] > target) {
      right = mid - 1;
    } else {
      if (mid == arr.length - 1 || arr[mid + 1] != target) {
        return mid;
      } else {
        left = mid + 1;
      }
    }
 }
 return -1;
};

// function name is bubbleSort
let bubbleSort = (arr) => {
 for (let i = 0; i < arr.length - 1; i++) {
    for (let j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        let temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
 }
 return arr;
};

// function name is bubbleSortOptimized
let bubbleSortOptimized = (arr) => {
 let isSwapped;
 for (let i = 0; i < arr.length - 1; i++) {
   isSwapped = false;
    for (let j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        let temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
        isSwapped = true;
      }
    }
    if (!isSwapped) {
      break;
    }
 }
 return arr;
};


// function name is heapSort
let heapSort = (arr) => {
 let n = arr.length;
 for (let i = Math.floor(n / 2) - 1; i >= 0; i--) {
    heapify(arr, n, i);
 }
 for (let i = n - 1; i >= 0; i--) {
    let temp = arr[0];
    arr[0] = arr[i];
    arr[i] = temp;
    heapify(arr, i, 0);
 }
 return arr;
};

let heapify = (arr, n, i) => {
 let largest = i;
 let left = 2 * i + 1;
 let right = 2 * i + 2;
 if (left < n && arr[left] > arr[largest]) {
    largest = left;
 }
 if (right < n && arr[right] > arr[largest]) {
    largest = right;
 }
 if (largest != i) {
    let swap = arr[i];
    arr[i] = arr[largest];
    arr[largest] = swap;
    heapify(arr, n, largest);
 }
};

// function name is quickSort
let quickSort = (arr, low, high) => {
 if (low < high) {
    let pi = partition(arr, low, high);
    quickSort(arr, low, pi - 1);
    quickSort(arr, pi + 1, high);
 }
};

let partition = (arr, low, high) => {
 let pivot = arr[high];
 let i = low - 1;
 for (let j = low; j <= high - 1; j++) {
    if (arr[j] < pivot) {
      i++;
      let temp = arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
    }
 }
 let temp = arr[i + 1];
 arr[i + 1] = arr[high];
 arr[high] = temp;
 return i + 1;
};

// function name is selectionSort
let selectionSort = (arr) => {
 let n = arr.length;
 for (let i = 0; i < n - 1; i++) {
    let minIndex = i;
    for (let j = i + 1; j < n; j++) {
      if (arr[j] < arr[minIndex]) {
        minIndex = j;
      }
    }
    let temp = arr[minIndex];
    arr[minIndex] = arr[i];
    arr[i] = temp;
 }
 return arr;
};

// function name is insertionSort
let insertionSort = (arr) => {
 for (let i = 1; i < arr.length; i++) {
    let key = arr[i];
    let j = i - 1;
    while (j >= 0 && arr[j] > key) {
      arr[j + 1] = arr[j];
      j = j - 1;
    }
    arr[j + 1] = key;
 }
 return arr;
};


let array = [5, 8, 1, 9, 2, 7, 6, 3, 4, 10, 12, 11, 14, 13, 16, 15, 18, 17, 20, 19];

console.log('Before sorting: ', array);
console.log('After sorting: ', bubbleSort(array));

function compareArrays(a, b) {
 if (a.length !== b.length) {
    return false;
 }
 for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      return false;
    }
 }
 return true;
}

let arraySortedByBubbleSort = bubbleSort(array);
let arraySortedBySelectionSort = selectionSort(array);
let arraySortedByInsertionSort = insertionSort(array);
let arraySortedByQuickSort = [...array];
quickSort(arraySortedByQuickSort, 0, arraySortedByQuickSort.length - 1);
let arraySortedByMergeSort = mergeSort(array);
let arraySortedByHeapSort = heapSort(array);

console.log('Bubble Sort and Selection Sort have the same output:', compareArrays(arraySortedByBubbleSort, arraySortedBySelectionSort));
console.log('Bubble Sort and Insertion Sort have the same output:', compareArrays(arraySortedByBubbleSort, arraySortedByInsertionSort));
console.log('Bubble Sort and Quick Sort have the same output:', compareArrays(arraySortedByBubbleSort, arraySortedByQuickSort));
console.log('Bubble Sort and Merge Sort have the same output:', compareArrays(arraySortedByBubbleSort, arraySortedByMergeSort));
console.log('Bubble Sort and Heap Sort have the same output:', compareArrays(arraySortedByBubbleSort, arraySortedByHeapSort));

let n = 100000;
let arr = Array.from({ length: n }, () => Math.floor(Math.random() * n));

let bubbleSortStart = performance.now();
bubbleSort(arr);
let bubbleSortEnd = performance.now();

let selectionSortStart = performance.now();
selectionSort(arr);
let selectionSortEnd = performance.now();

let insertionSortStart = performance.now();
insertionSort(arr);
let insertionSortEnd = performance.now();

let quickSortStart = performance.now();
quickSort(arr, 0, arr.length - 1);
let quickSortEnd = performance.now();

let mergeSortStart = performance.now();
mergeSort(arr);
let mergeSortEnd = performance.now();

let heapSortStart = performance.now();
heapSort(arr);
let heapSortEnd = performance.now();

console.log('Bubble Sort Time:', bubbleSortEnd - bubbleSortStart);
console.log('Selection Sort Time:', selectionSortEnd - selectionSortStart);
console.log('Insertion Sort Time:', insertionSortEnd - insertionSortStart);
console.log('Quick Sort Time:', quickSortEnd - quickSortStart);
console.log('Merge Sort Time:', mergeSortEnd - mergeSortStart);
console.log('Heap Sort Time:', heapSortEnd - heapSortStart);

console.log('All tests passed successfully!');