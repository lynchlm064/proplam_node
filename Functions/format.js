import fs from "fs";
import { exec } from "child_process";
import clc from "cli-color";
// ... Your token, botOwnerID, and other configurations here
export function formatBotCode(error, stdout, stderr) {
  exec("npx prettier --write Functions/format.js");
  fs.readFile("index.js", "utf8", (err, data, error, stdout, stderr) => {
    fs.writeFileSync("backup/[bac]index.js", data);
    exec("npx prettier --write index.js", (error, stdout, stderr) => {
      console.log(
        clc.red(`Code formatted and backed up successfully: ${stdout}`),
      );
    });
  });
}
