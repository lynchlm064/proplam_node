import axios from "axios";
import dotenv from 'dotenv';
import fs from "fs";
import FormData from "form-data";
import path from "path";
import cliColor from "cli-color";
import clc from "cli-color";

dotenv.config();

const hook = process.env.PROPLAM_HOOK;
const webhookUrl = hook;

// requires both a message and file path argument \\
export async function sendFunnyFile(message, filePath) {
 try {
   // Read the file as a Buffer
   const fileData = fs.readFileSync(filePath);
   const fileName = path.basename(filePath);
   const fileStats = fs.statSync(filePath);
   const fileSizeInBytes = fileStats.size;
   const fileSizeInKb = fileSizeInBytes / 1024; // Convert bytes to KB

   console.log(`File size: ` + 
   clc.red(`${fileSizeInBytes}`) + ` bytes (` + 
   clc.red(`${fileSizeInKb}`) + ` KB)`);
   // Create a FormData object to send as a multipart/form-data request
   const formData = new FormData();
   formData.append('file', fileData, { filename: fileName });
   formData.append('content', message)
   // Send the file to the Discord webhook using Axios \\
   const response = await axios.post(webhookUrl, formData, {
     headers: {
       ...formData.getHeaders(),
     },
   });
  // Check the response \\
  console.log(`webhook status: ` + clc.blue(`${response.status}`) + ` - ` + clc.red(`${response.statusText}`));
} catch (error) {
  console.error(`Error sending file to Discord: ` + clc.red(`${error.message}`));
}
}

// just for messages \\
   export async function sendFunnyMessage(message) {
    try {
    await axios.post(webhookUrl, { content: message });
    } catch (error) {
    console.error("Error lol:", error.message);
    }
    console.log("webhook message sent: " + clc.red(message));
   }

// js does not have an inbuilt sleep function \\
export function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
