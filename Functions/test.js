//convert everything here to lua game guardian and instead of using axios for js use game guardians gg.makeRequest function 
import axios from "axios"; //require("axios");

async function obfuscator(code) {
  const response = await axios.post("https://luaobfuscator.com/api/ide/init?f=5", code);
  await new Promise((resolve) => setTimeout(resolve, 1000));
  const sessionId = response.data.sessionId;
  const obfuscator = axios.create({
    headers: {
      sessionId: sessionId
    }
  });
  const obfuscateResponse = await obfuscator.post("https://luaobfuscator.com/api/ide/obfuscateAll/pre-2/2");
  const tokens = obfuscateResponse.data.tokens;
  let proplam = "";
  for (let i = 0; i < tokens.length; i++) {
    proplam += tokens[i].value;
  }
       const herx = `
 --\[\[
 
    ██╗  ██╗███████╗██████╗ ███╗██╗  ██╗███╗
    ██║  ██║██╔════╝██╔══██╗██╔╝╚██╗██╔╝╚██║
    ███████║█████╗  ██████╔╝██║  ╚███╔╝  ██║
    ██╔══██║██╔══╝  ██╔══██╗██║  ██╔██╗  ██║
    ██║  ██║███████╗██║  ██║███╗██╔╝ ██╗███║
    ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚══╝╚═╝  ╚═╝╚══╝

    [https://luaobfuscator.com]
 \]\]--
  `;
 const prop = proplam.split("\n")[11];
 const ret =  herx + "\n" + prop;
 return ret;
}
