import fs from 'fs';
import JavaScriptObfuscator from 'javascript-obfuscator';

export const obfuscateFile = (filePath) => {
  try {
    // Read the content of the file
    const code = fs.readFileSync(filePath, 'utf8');

    // Obfuscate the code
    const obfuscatedCode = JavaScriptObfuscator.obfuscate(code, {
      compact: false,
      controlFlowFlattening: true,
      controlFlowFlatteningThreshold: 1,
      numbersToExpressions: true,
      simplify: true,
      shuffleStringArray: true,
      splitStrings: true,
      stringArrayThreshold: 1,
    }).getObfuscatedCode();

    // Write the obfuscated code b back to the file
    fs.writeFileSync(filePath, obfuscatedCode, 'utf8');

    console.log(`File '${filePath}' has been obfuscated.`);
  } catch (error) {
    console.error(`Error obfuscating the file: ${error}`);
  }
}