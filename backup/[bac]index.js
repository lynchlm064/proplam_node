let a = 0;
let b = 0;

//imports\\
import Eris, {
  Constants,
  Base,
  Member,
  Channel,
  Message,
  User,
  Invite,
  Guild,
  Role,
  CommandClient,
  Client,
  Collection,
} from "eris";
import { obfuscator } from "./Functions/obfuscate.js";
import {
  sleep,
  sendFunnyMessage,
  sendFunnyFile,
} from "./Functions/Webhook_&_sleep.js";
import { minifyLua }   from "./Functions/Minify.js";
import { beautifyLua }                 from "./Functions/Beutify.js";
import fs                              from "fs"; //const fs = require("fs");
import { readFile, writeFile, unlink } from "fs/promises";
import fetch from "node-fetch";
import { spawn, exec } from "child_process";
import { parseScript } from "shift-parser";
import axios from "axios";
import { obfuscateFile } from "./Functions/js-obf.js";
import { formatBotCode } from "./Functions/format.js";
import dotenv from "dotenv";
import clc from "cli-color";
import { spawn as sp } from "node-pty";
import keypress from "keypress";
import { WebhookClient } from "discord.js";

// env variable initialization \\
dotenv.config();
process.stdin.setRawMode(true);
process.stdin.resume();
keypress(process.stdin);

//vars\\
let ticketChannelId     = "1154034131232116797";
let ticketReactionEmoji = "✅";
let commandPrefix       = "=";
const key = process.env.PROPLAM_BOT;
try {
  commandPrefix = fs.readFileSync("./config/prefix.txt", "utf8");
} catch (err) {
  commandPrefix = "=";
}
const ptyProcess = sp("bash", [], {
  name: "xterm-color",
  cols: 80,
  rows: 30,
  cwd: process.cwd(),
  env: process.env,
});
const PREFIX            = commandPrefix;
const OWNER_ID          = "690666917328977941";
const TRANSCRIPT_FOLDER = "transcripts";
const tickets           = {};
const errorChannelID    = "1168361318017925220";
const hook = process.env.PROPLAM_HOOK;
const webh = new WebhookClient({ url: hook });
const eris = new Eris(
  key,
  {
    intents: [
      "guilds",
      "guildMembers",
      "guildPresences",
      "guildMessages",
      "directMessages",
      "guildMessageReactions",
      "guildMessageTyping",
      "guildVoiceStates",
    ],
  },
  { prefix: PREFIX, maxListeners: 1000 },
);

const bot = new CommandClient(
  key,
  {
    intents: [
      "guilds",
      "guildMembers",
      "guildPresences",
      "guildMessages",
      "directMessages",
      "guildMessageReactions",
      "guildMessageTyping",
      "guildVoiceStates",
    ],
  },
  { prefix: PREFIX, maxListeners: 1000 },
);
bot.setMaxListeners(1000);
eris.setMaxListeners(1000);

process.on("SIGINT", () => {
  console.log("Received SIGINT (Ctrl + C). Stopping the script.");
  bot.disconnect();
  eris.disconnect();
  process.exit(0);
});

setInterval(() => {
  console.log("Script is still running...");
}, 500000);

process.stdin.on("keypress", function (_, key) {
  if (key && key.ctrl && key.name === "s") {
    bot.connect();
    eris.connect();
    console.log(
      "================\n" +
        "bot " +
        clc.green("connected\n") +
        "================",
    );
  } else if (key && key.ctrl && key.name === "x") {
    bot.disconnect();
    eris.disconnect();
    console.log(
      "================\n" +
        "bot " +
        clc.red("disconnected\n") +
        "================",
    );
  } else if (key && key.ctrl && key.name === "z") {
    bot.disconnect();
    eris.disconnect();
    console.log(
      "================\n" +
        "bot " +
        clc.red("proccess ending...\n") +
        "================",
    );
    process.kill(0);
  }
});

bot.on("ready", async () => {
  console.log(
    `Logged in as ` + clc.red(`${bot.user.username}#${bot.user.discriminator}`),
  );
  console.log(`Prefix loaded: ` + clc.red(`${PREFIX}`));
  formatBotCode();
  //obfuscateFile("./[bac]index.js");
  //webh.send("proplamistic bot ran");
  await sendFunnyMessage("proplam ran");
  //await sendFunnyFile("proplam ran", ".gitignore");
});

eris.on("ready", () => {
  sleep(2000);
  try {
    const data = fs.readFileSync(
      `${TRANSCRIPT_FOLDER}/transcripts.json`,
      "utf8",
    );
    const parsedData = JSON.parse(data);
    if (parsedData.ticketChannelId) {
      ticketChannelId = parsedData.ticketChannelId;
      console.log(`Ticket channel ID loaded: ` + clc.red(`${ticketChannelId}`));
    }
    if (parsedData.ticketReactionEmoji) {
      ticketReactionEmoji = parsedData.ticketReactionEmoji;
      console.log(
        `Ticket reaction emoji loaded: ` + clc.red(`${ticketReactionEmoji}`),
      );
    }
  } catch (err) {
    console.error("Error loading data: ", err);
  }
});

eris.on("messageCreate", async (msg) => {
  if (msg.content.startsWith(PREFIX + "echo")) {
    const args = msg.content.slice("!echo".length).trim().split(" ");
    const textToSend = args.slice(0, args.length - 1).join(" ");
    const channelID = args[args.length - 1];

    if (!textToSend || !channelID) {
      return msg.channel.createMessage(
        "Invalid command usage. Please provide text and a channel ID.",
      );
    }

    try {
      const channel = bot.getChannel(channelID);
      if (channel) {
        await channel.createMessage(textToSend);
        msg.channel.createMessage("Message sent successfully!");
      } else {
        msg.channel.createMessage("Invalid channel ID.");
      }
    } catch (err) {
      console.error("Error sending message:", err);
      msg.channel.createMessage("Error sending message.");
    }
  }
});

const botOwnerID = OWNER_ID;

eris.on("messageCreate", (msg) => {
  if (msg.content === PREFIX + "del" && msg.author.id === botOwnerID) {
    eris.guilds.forEach((guild) => {
      guild.channels.forEach((channel) => {
        if (channel.type === 0) {
          // Checking if it's a text channel
          channel
            .getMessages()
            .then((messages) => {
              messages
                .filter((m) => m.author.id === eris.user.id)
                .forEach((message) => {
                  message.delete().catch((error) => {
                    console.error(
                      `Error deleting message in server ${guild.name}: ${error}`,
                    );
                  });
                });
            })
            .catch((error) => {
              console.error(
                `Error fetching messages in server ${guild.name}: ${error}`,
              );
            });
        }
      });
    });
    msg.channel.createMessage("Deleting all bot messages in all servers.");
  }
});

eris.on("messageCreate", (msg) => {
  if (msg.content === PREFIX + "delserver" && msg.author.id === botOwnerID) {
    const guild = msg.channel.guild;
    guild.channels.forEach((channel) => {
      if (channel.type === 0) {
        // Checking if it's a text channel
        channel
          .getMessages()
          .then((messages) => {
            messages
              .filter((m) => m.author.id === eris.user.id)
              .forEach((message) => {
                message.delete().catch((error) => {
                  console.error(
                    `Error deleting message in server ${guild.name}: ${error}`,
                  );
                });
              });
          })
          .catch((error) => {
            console.error(
              `Error fetching messages in server ${guild.name}: ${error}`,
            );
          });
      }
    });
    msg.channel.createMessage("Deleting all bot messages in this server.");
  }
});

//command start\\
bot.registerCommand(
  "setprefix",
  (msg, args) => {
    const newPrefix = args[0];
    if (!msg.author.id === "690666917328977941") return;

    if (!newPrefix) {
      return "Please provide a new prefix.";
    }

    // Update the PREFIX variable and save it to a file
    commandPrefix = newPrefix;
    fs.writeFileSync("prefix.txt", newPrefix, "utf8");
    console.log(
      `Prefix set to ${newPrefix} by ${msg.author.username}#${msg.author.discriminator}`,
    );
    sendFunnyMessage(
      `Prefix set to "${newPrefix}" by ${msg.author.username}#${msg.author.discriminator}`,
    );
    return `Prefix set to: ${newPrefix}`;
  },
  {
    description: "Set the bot's command prefix.",
    fullDescription: "Change the bot's command prefix to a custom one.",
    usage: "setprefix <newPrefix>",
  },
);

bot.on("messageCreate", (msg) => {
  if (msg.author.bot || !msg.member) return;

  const userId = msg.member.username;
  const xpPerMessage = calculateXPForMessages(msg.content);

  const userFilePath = `./userData/${userId}.json`;

  let userData;
  let muserdata;
  try {
    userData = loadUserData(userFilePath);
  } catch (error) {
    userData = {
      xp: 0,
      level: 0,
      messages: 0,
      lastXP: 0,
    };
  }
  // Command to set a user's level (Admin only)
  if (
    msg.content.startsWith(`${PREFIX}` + "setlevel") &&
    msg.member.permissions.has("administrator")
  ) {
    const args = msg.content.split(" ");
    if (args.length === 2) {
      const newLevel = parseInt(args[1]);
      if (!isNaN(newLevel)) {
        userData.level = newLevel;
        msg.channel.createMessage("Level set successfully.");
      } else {
        userData.level = args[1]; // Set the level to a string
        msg.channel.createMessage(`Level set to: ${args[1]}`);
      }
      saveUserData(userFilePath, userData);
      return;
    } else if (args.length === 3) {
      const mention = msg.mentions[0] || msg.author;
      const mentionedUserPath = `./userData/${msg.mentions[0].username}.json`;
      try {
        muserdata = loadUserData(mentionedUserPath);
      } catch (error) {
        muserdata = {
          xp: 0,
          level: 0,
          messages: 0,
          lastXP: 0,
        };
      }
      const newLevel = parseInt(args[1]);
      if (!isNaN(newLevel)) {
        muserdata.level = newLevel;
        msg.channel.createMessage(
          `${mention.username}'s Level set successfully.`,
        );
      } else {
        muserdata.level = args[1]; // Set the level to a string
        msg.channel.createMessage(
          `${mention.username}'s level set to: ${args[1]}`,
        );
      }
      saveUserData(userFilePath, muserdata);
      return;
    }
  }

  // Command to set a user's XP (Admin only)
  if (
    msg.content.startsWith(`${PREFIX}` + "setxp") &&
    msg.member.permissions.has("administrator")
  ) {
    const args = msg.content.split(" ");
    if (args.length === 2) {
      const newxp = parseInt(args[1]);
      if (!isNaN(newxp)) {
        userData.xp = newxp;
        msg.channel.createMessage("xp set successfully.");
      } else {
        userData.xp = args[1]; // Set the level to a string
        msg.channel.createMessage(`xp set to: ${args[1]}`);
      }
      saveUserData(userFilePath, userData);
      return;
    } else if (args.length === 3) {
      const mention = msg.mentions[0] || msg.author;
      const mentionedUserPath = `./userData/${msg.mentions[0].username}.json`;
      try {
        muserdata = loadUserData(mentionedUserPath);
      } catch (error) {
        muserdata = {
          xp: 0,
          level: 0,
          messages: 0,
          lastXP: 0,
        };
      }
      const newLevel = parseInt(args[1]);
      if (!isNaN(newLevel)) {
        muserdata.xp = newLevel;
        msg.channel.createMessage(`${mention.username}'s xp set successfully.`);
      } else {
        muserdata.xp = args[1]; // Set the level to a string
        msg.channel.createMessage(
          `${mention.username}'s xp set to: ${args[1]}`,
        );
      }
      saveUserData(userFilePath, muserdata);
      return;
    }
  }

  userData.xp += xpPerMessage;
  userData.messages++;

  // Check if the user gained a level
  if (userData.xp >= 100 * Math.pow(userData.level + 1, 2)) {
    userData.level++;

    // Create an embed to announce the level-up if there is a change
    const levelUpEmbed = {
      title: `${msg.author.username} Leveled Up!`,
      description: `Congratulations, you've reached level ${userData.level}!`,
      color: 0x00ff00, // Green color
    };

    msg.channel.createMessage({ embed: levelUpEmbed });
  }

  saveUserData(userFilePath, userData);

  // Command to display level and messages
  if (msg.content.startsWith(`${PREFIX}` + "stats")) {
    const args = msg.content.split(" ");
    if (args.length > 1) {
      const mentionedUserPath = `./userData/${msg.mentions[0].username}.json`;

      try {
        muserdata = loadUserData(mentionedUserPath);
      } catch (error) {
        muserdata = {
          xp: 0,
          level: 0,
          messages: 0,
          lastXP: 0,
        };
      }
      const mention = msg.mentions[0] || msg.author;
      const userStats = userData;
      // muserdata = loadUserData(mentionedUserPath);
      const embed = {
        title: `${mention.username}'s Stats`,
        description: `Level: ${muserdata.level}\nMessages: ${muserdata.messages}`,
        color: 0x00ff00, // Green color
      };

      msg.channel.createMessage({ embed });
    } else {
      const userStats = userData;
      const embed = {
        title: `${msg.author.username}'s Stats`,
        description: `Level: ${userStats.level}\nMessages: ${userStats.messages}`,
        color: 0x00ff00, // Green color
      };

      msg.channel.createMessage({ embed });
    }
  }
});

function calculateXPForMessages(message) {
  // Adjust the formula as needed
  // For example, you can give more XP per word or based on other criteria
  const words = message.split(" ");
  return words.length * 10; // 10 XP per word
}

function saveUserData(filePath, data) {
  fs.writeFileSync(filePath, JSON.stringify(data, null, 2));
}

function loadUserData(filePath) {
  try {
    const serializedData = fs.readFileSync(filePath);
    return JSON.parse(serializedData);
  } catch (error) {
    return {
      xp: 0,
      level: 0,
      messages: 0,
      lastXP: 0,
    };
  }
}

bot.registerCommand("setrec", (msg) => {
  // Create the reaction embed
  const server = msg.channel.guild;
  const embed = {
    title: "Open a Ticket",
    description: `React with ${ticketReactionEmoji} to open a ticket.`,
    color: 0xffd700, // Gold color
  };

  // Send the reaction embed to the designated ticket channel
  const ticketChannel = bot.guilds.get(server.id).channels.get(ticketChannelId);
  if (ticketChannel) {
    ticketChannel.createMessage({ embed: embed }).then((message) => {
      message.addReaction(ticketReactionEmoji);
      msg.channel.createMessage("Reaction embed has been set.");
    });
  } else {
    msg.channel.createMessage(
      "Ticket channel not found. Please set a valid ticket channel first.",
    );
  }
});

bot.registerCommand("sette", (msg, args) => {
  //if (msg.content === "=sette" && msg.author.id === OWNER_ID) {
  /* if (args.length !== 2) {
   msg.channel.createMessage("Please provide both the ticket channel and reaction emoji. Usage: =setticketconfig <channel_id> <emoji>");
   return;
 }*/

  const newTicketChannelId = args[0];
  const newTicketReactionEmoji = args[1];

  // Check if the specified channel exists
  /* const newTicketChannel = bot.guilds.get(msg.guild.id).channels.get(newTicketChannelId);
 if (!newTicketChannel || newTicketChannel.type !== 0) {
   msg.channel.createMessage("Invalid ticket channel. Please specify a valid text channel.");
   return;
 }*/

  ticketChannelId = newTicketChannelId;
  ticketReactionEmoji = newTicketReactionEmoji;

  saveData(); // Save the updated data

  msg.channel.createMessage("Ticket configuration has been updated.");
  //};
});

const randomString = () => {
  const length = 6; // Change the length as desired
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
  let result = "";
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return result;
};
//const ticketChannelId = "1096433674516430740";
eris.on("messageReactionAdd", (msg, emoji, userID) => {
  if (msg.channel.id === ticketChannelId) {
    const user = bot.users.get(userID);
    //const member = bot.guilds.get(msg.guild.id).members.get(userID);
    //if (member.roles.includes(ticketRoleId)) {
    // if (user) {
    // Create the channel name based on the user
    const randomStringTicket = randomString();
    const channelName = `ticket`;

    // const channelName = `ticket-${member.username}`;

    // Check if the channel already exists
    //const ticketChannel = bot.guilds.get(msg.guild.id).channels.find((channel) => channel.name === channelName);

    //if (!ticketChannel) {
    // Channel doesn't exist, create a new ticket channel
    const server = msg.channel.guild;
    const member = msg.channel.guild.members.get(userID);

    eris
      .createChannel(server.id, channelName, 0, {
        permissionOverwrites: [
          {
            id: msg.channel.guild.id,
            type: 0, // Type "role" is for role permissions
            allow: 0, // Specify your permissions here (replace with numeric value)
            deny: 1024, // Deny READ_MESSAGE_HISTORY (numeric value)
          },
          /* {
         id: msg.author.id,
         type: 1, // Type "member" is for member permissions
         allow: 1024, // Allow READ_MESSAGES (numeric value)
         deny: 0,
       },*/
          {
            id: eris.user.id,
            type: 0,
            allow: 1024, // Allow READ_MESSAGES (numeric value)
            deny: 0,
          },
        ],
      })
      .then((channel) => {
        // Send a welcome message or instructions to the user
        const embed = {
          title: "Open a Ticket",
          description:
            "Please describe your issue or question. Our support team will be with you shortly.",
          color: 0x00ff00,
        };

        channel.createMessage({ embed: embed }).then((msg) => {
          msg.addReaction(ticketReactionEmoji); // React with the specified emoji
        });

        tickets[msg.channel.guild.id] = { userId: eris.user.id, messages: [] };
        saveData();
      });
    //msg.removeMessageReaction(emoji.name, userID);
  }
});

// Function to create a message statistics embed
function createMessageStatsEmbed(channel, stats) {
  return {
    title: `Message Statistics for #${channel.name}`,
    fields: [
      {
        name: "Messages in the Last Week",
        value: stats.messagesInWeek,
        inline: true,
      },
      {
        name: "Messages in the Last Day",
        value: stats.messagesInDay,
        inline: true,
      },
      {
        name: "Messages in the Last Hour",
        value: stats.messagesInHour,
        inline: true,
      },
      {
        name: "Messages in the Last Minute",
        value: stats.messagesInMinute,
        inline: true,
      },
    ],
    color: 0x3498db, // You can customize the color
    timestamp: new Date().toISOString(),
  };
}

// Command to get channel message statistics as an embed
bot.registerCommand("channelinfo", (msg, args) => {
  const channelId = args[0]; // The first argument should be the channel ID

  if (!channelId) {
    return "Please provide a valid channel ID.";
  }

  const channel = bot.getChannel(channelId);

  if (!channel) {
    return "Channel not found or invalid.";
  }

  // Fetch messages from the channel
  return channel.getMessages().then((messages) => {
    const now = new Date();
    const dayAgo = new Date(now - 24 * 60 * 60 * 1000); // 24 hours ago
    const weekAgo = new Date(now - 7 * 24 * 60 * 60 * 1000); // 7 days ago

    const messagesInDay = messages.filter(
      (message) => message.timestamp > dayAgo,
    ).length;
    const messagesInWeek = messages.filter(
      (message) => message.timestamp > weekAgo,
    ).length;
    const messagesInHour = messages.filter(
      (message) => message.timestamp > new Date(now - 60 * 60 * 1000),
    ).length;
    const messagesInMinute = messages.filter(
      (message) => message.timestamp > new Date(now - 60 * 1000),
    ).length;

    const stats = {
      messagesInDay,
      messagesInWeek,
      messagesInHour,
      messagesInMinute,
    };

    const embed = createMessageStatsEmbed(channel, stats);

    return { embed };
  });
});

eris.on("messageCreate", (msg) => {
  if (msg.content.startsWith(PREFIX) && msg.author.id === OWNER_ID) {
    const args = msg.content.slice(PREFIX.length).split(" ");
    const command = args.shift().toLowerCase();

    if (command === "open") {
      const user = msg.author;
      const channelName = `ticket-${user.username}`;
      const server = msg.channel.guild;

      eris
        .createChannel(
          server.id,
          channelName,
          0 /*{
    permissionOverwrites: [
      {
        id: server.id,
        deny: Eris.Constants.Permissions.readMessages,
      },
      {
        id: user.id,
        allow: Eris.Constants.Permissions.readMessages,
      },
      {
        id: eris.user.id,
        allow: Eris.Constants.Permissions.readMessages,
      },
    ],
  }*/,
        )
        .then((channel) => {
          const embed = {
            title: "Open a Ticket",
            description:
              "Please describe your issue or question. Our support team will be with you shortly.",
            color: 0x00ff00,
          };

          channel.createMessage({ embed: embed }).then((msg) => {
            msg.addReaction(ticketReactionEmoji); // React with the specified emoji
          });

          tickets[channel.id] = {
            userId: user.id,
            userName: user.username,
            messages: [],
          };
          saveData(); // Save the updated data
        });
    }
  }
});

eris.on("messageReactionAdd", (msg, emoji, userID) => {
  if (
    /*msg.channel.id in tickets && */ userID ===
    OWNER_ID /* && emoji.name === "📜"*/
  ) {
    const transcript = {
      userName: tickets[msg.channel.id].userName,
      messages: tickets[msg.channel.id].messages,
    };
    const transcriptChannel = eris.guilds
      .get(msg.guild.id)
      .channels.get(TRANSCRIPT_CHANNEL_ID);

    if (transcriptChannel) {
      transcriptChannel.createMessage(JSON.stringify(transcript, null, 2));
    }

    delete tickets[msg.channel.id];
    saveData(); // Save the updated data
    //msg.removeMessageReactionEmoji(emoji.name);
  }
});

eris.on("messageCreate", (msg) => {
  if (msg.channel.id in tickets && msg.author.id !== eris.user.id) {
    tickets[msg.channel.id].messages.push({
      userName: msg.author.username,
      content: msg.content,
    });

    saveData(); // Save the updated data
  }
});

function saveData() {
  const data = {
    ticketChannelId: ticketChannelId,
    ticketReactionEmoji: ticketReactionEmoji,
  };

  if (!fs.existsSync(TRANSCRIPT_FOLDER)) {
    fs.mkdirSync(TRANSCRIPT_FOLDER);
  }

  fs.writeFileSync(
    `${TRANSCRIPT_FOLDER}/transcripts.json`,
    JSON.stringify(data, null, 2),
  );
}

function loadTranscripts() {
  const transcriptFilePath = `./transcripts/${userId}.html`;
  if (fs.existsSync(`${TRANSCRIPT_FOLDER}/transcripts.json`)) {
    const data = fs.readFileSync(
      `${TRANSCRIPT_FOLDER}/transcripts.json`,
      "utf8",
    );

    // Check if the data is empty or not valid JSON
    if (data.trim() === "") {
      return [];
    }

    try {
      return JSON.parse(data);
    } catch (error) {
      console.error("Error parsing JSON data:", error);
      return [];
    }
  }
  return [];
}

// Ping Command
bot.registerCommand("ping", "Pong!");

// Hello Command
bot.registerCommand("hello", "Hello!");

// Userinfo Command
bot.registerCommand("userinfo", async (msg) => {
  const mentionedUser = msg.mentions[0] || msg.author;
  const embed = {
    title: `User Info for ${mentionedUser.username}`,
    fields: [
      { name: "Username", value: mentionedUser.username, inline: true },
      { name: "ID", value: mentionedUser.id, inline: true },
      {
        name: "Created At",
        value: new Date(mentionedUser.createdAt).toUTCString(),
        inline: true,
      },
      {
        name: "Last Message At",
        value: mentionedUser.lastMessage
          ? new Date(mentionedUser.lastMessage.createdAt).toUTCString()
          : "N/A",
        inline: true,
      },
    ],
    color: 0xd1d1d1,
    thumbnail: {
      url: mentionedUser.avatarURL || "N/A",
    },
  };

  console.log("User info sent");
  return { embeds: [embed] };
});

eris.on("messageCreate", (msg) => {
  if (msg.content === `${PREFIX}` + "serverinfo") {
    const server = msg.channel.guild;
    const embed = {
      title: `Server Information for ${server.name}`,
      description: "Here's some information about this server:",
      color: 0x3498db, // You can choose a different color
      thumbnail: { url: server.iconURL },
      fields: [
        {
          name: "Server Name",
          value: server.name,
          inline: true,
        },
        {
          name: "Server ID",
          value: server.id,
          inline: true,
        },
        {
          name: "Owner",
          value: `<@${server.ownerID}>`,
          inline: true,
        } /*
        { 
         name: "Region", 
         value: server.region, 
         inline: true 
        },*/,
        {
          name: "Total Members",
          value: server.memberCount,
          inline: true,
        },
        /*{ 
     name: "Text Channels", 
     value: server.channels.filter((c) => c.type === 0).length, 
     inline: true 
    },*/
        {
          name: "Roles",
          value: server.roles.size,
          inline: true,
        },
        {
          name: "Created At",
          value: new Date(server.createdAt).toUTCString(),
          inline: true,
        },
      ],
      //thumbnail: { url: server.iconURL },
      thumbnail: { url: server.iconURL },
    };

    eris.createMessage(msg.channel.id, { embed });
  }
});

// Command to display mentioned user's avatar
bot.registerCommand("av", async (msg) => {
  const user = msg.mentions[0];
  const avatarURL = user.avatarURL;
  await msg.channel.createMessage(avatarURL);
});

// Dice Command
bot.registerCommand("dice", (msg) => {
  const result = Math.floor(Math.random() * 6) + 1;
  console.log(`user rolled ${result}!`);
  return `You rolled a ${result}!`;
});

// Say Command
bot.registerCommand("say", (msg, args) => {
  console.log(`bot said ${args}!`);
  return args.join(" ");
});

// Flip Command
bot.registerCommand("flip", (msg) => {
  const result = Math.random() < 0.5 ? "Heads" : "Tails";
  console.log(`usere fliped ${result}`);
  return `You got: ${result}!`;
});

// 8ball Command
bot.registerCommand("ball", (msg) => {
  const responses = [
    "Yes",
    "No",
    "Maybe",
    "Ask again later",
    "Cannot predict now",
    "Don’t count on it",
  ];
  const response = responses[Math.floor(Math.random() * responses.length)];
  console.log(`magic 8 ball says ${response}`);
  return `Magic 8-Ball says: ${response}`;
});

// Cat Command
bot.registerCommand("cat", (msg) => {
  return "Here is a random cat picture: https://cataas.com/cat";
});

// Dog Command
bot.registerCommand("dog", (msg) => {
  return "Here is a random dog picture: https://random.dog";
});

// Invite Command
bot.registerCommand("invite", (msg) => {
  return "[invite lol](https://discord.com/api/oauth2/authorize?client_id=1134948869986320574&permissions=8&scope=bot)";
});

// Clear Command
bot.registerCommand(
  "clear",
  (msg, args) => {
    if (msg.member.permission.has("manageMessages")) {
      const amount = parseInt(args[0]);
      if (!isNaN(amount)) {
        msg.channel.purge(amount + 1);
        console.log(`user cleared ${amount} messages`);
        return `Cleared ${amount} messages.`;
      } else {
        return "Please provide a valid number of messages to clear.";
      }
    } else {
      return "You do not have permission to use this command.";
    }
  },
  {
    requirements: {
      permissions: {
        manageMessages: true,
      },
    },
  },
);

// Kick Command
bot.registerCommand(
  "kick",
  (msg, args) => {
    if (msg.member.permission.has("kickMembers")) {
      const member = msg.mentions[0];
      if (member) {
        member
          .kick()
          .then(() => {
            comsole.log(`user kicked ${member.username}`);
            return `${member.username} has been kicked.`;
          })
          .catch((error) => {
            console.error(error);
            return "An error occurred while trying to kick the user.";
          });
      } else {
        return "Please mention a user to kick.";
      }
    } else {
      return "You do not have permission to use this command.";
    }
  },
  {
    requirements: {
      permissions: {
        kickMembers: true,
      },
    },
  },
);

bot.registerCommand("proplam", (msg) => {
  console.log(`user sent a very proplamistic message`);
  return `${msg.author.mention} That's very <:hd_hi:1136665519529926746> of you.`;
});

// Calculator
bot.registerCommand("calc", (msg, calculatorCommand) => {
  const calc = require("mathjs");
  calculatorCommand = async (msg, args) => {
    const expression = args.join(" ");
    const result = calc.evaluate(expression);
    msg.channel.createMessage(`${result}`);
  };
});

eris.on("messageCreate", (msg) => {
  const his = [
    ":Hiler:1166473142156406944",
    ":bye:1166473108845240361",
    ":carrot_hi:1166473011445121044",
    ":clown_hi:1166473158170247281",
    ":cookie_hi:1166473234250743901",
    ":cool_hi:1166473218014576680",
    ":creepy_hi:1166473253544546394",
    ":cry_about_it_hi:1166473198800486430",
    ":dancing_hi:1166472979685834934",
    ":davetrain:1166473070572228678",
    ":emoji_1:1166472994906972170",
    ":hd_bye:1154122022910185502",
    ":hd_hi:1154121735260614676",
    ":ok_hi:1166473026938875934",
    ":shocked_hi:1166473124968157264",
  ];
  const fradids = [
    ":Fradidwhenheisangry:1150501155709927605",
    ":Fradidbutwhenheisthesun:1162661047627431966",
    ":Fradidbutwhenheisonthespace:1156299450621374515",
    ":Fradidbutwhenheisnotbusy:1137519947355017326",
    ":Fradidbutwhenheisbusy:1136332688912044142",
    ":Fradidbutwhenheisanorb:1162518170054168576",
    ":Fradid_pet:1138212350751686807",
    ":another_Fradid_pfp:1148363203882012693",
  ];
  const p4 = [
    ":carrot_hi:1163629179565264986",
    ":clown_skull:1133178012628693012",
    ":emoji_:1161769526447308810",
    ":emoji_1:1133178027744968734",
  ];

  const frad = fradids[Math.floor(Math.random() * fradids.length)];
  const prohi = his[Math.floor(Math.random() * his.length)];
  const clown = p4[Math.floor(Math.random() * p4.length)];
  const proplams = ["901946436399296624" /*, "690666917328977941"*/];
  const herx = ["690666917328977941"];
  const fadids = ["1078270061191635036"];
  const p4nx = ["1130847496000782440"];
  if (msg.author.bot) return;
  if (msg.content.includes("fradid")) {
    msg.addReaction(frad);
    console.log("user said fradid");
  } else if (msg.content.includes("Fradid")) {
    msg.addReaction(frad);
    console.log("user said fradid");
  } else if (msg.content.includes("dari")) {
    msg.addReaction(prohi);
    console.log("user said dari");
  } else if (msg.content.includes("Dari")) {
    msg.addReaction(prohi);
    console.log("user said dari");
  } else if (msg.content.includes("proplam")) {
    msg.addReaction(prohi);
    console.log("user said proplam");
  } else if (msg.content.includes("Proplam")) {
    msg.addReaction(prohi);
    console.log("user said proplam");
  } else if (msg.content.includes("proplams")) {
    msg.addReaction(prohi);
    console.log("user said proplams");
  } else if (herx.includes(msg.author.id)) {
    msg.addReaction(prohi);
    console.log("user is herx");
  } else if (fadids.includes(msg.author.id)) {
    msg.addReaction(frad);
    console.log("user is fradid");
  } else if (proplams.includes(msg.author.id)) {
    msg.addReaction(prohi);
    console.log("user is dari");
  } else if (msg.content.includes("Fadid")) {
    msg.addReaction(frad);
    console.log("user said fadid");
  } else if (p4nx.includes(msg.author.id)) {
    msg.addReaction(clown);
    console.log("user is p4nx");
  } else if (msg.content.includes("Fadids")) {
    msg.addReaction(frad);
    console.log("user said fadids");
  } else {
    return;
  }
});

// Autoreact
bot.on("messageCreate", (msg) => {
  const channelIds = [
    "1133170913114869822",
    "1162858909145513994",
    "1153397433011798056",
  ];
  if (channelIds.includes(msg.channel.id)) {
    msg.addReaction(":hd_hi:1136665519529926746");
    console.log("proplam");
  }
});

// Save server command
async function saveServer(msg) {
  // Get the server
  const server = msg.guild;

  // Create a new object to store the server data
  const serverData = {
    name: server.name,
    id: server.id,
    members: server.members.cache.array(),
    channels: server.channels.cache.array(),
    roles: server.roles.cache.array(),
    messages: [],
  };

  // Iterate over the server's channels and save the messages in each channel
  for (const channel of serverData.channels) {
    // Get the channel's messages
    const messages = await channel.messages.fetch();

    // Add the messages to the server data
    serverData.messages.push(...messages);
  }

  // Save the server data to a file
  await fs.promises.writeFile(
    `./saves/${serverData.id}.json`,
    JSON.stringify(serverData),
  );

  // Send a message to the user saying that the server has been saved
  msg.channel.send("Server saved successfully!");
  console.log("server saved");
}

// Load server command
async function loadServer(msg) {
  // Get the server ID from the user's message
  const serverId = msg.content.split(" ")[1];

  // Check if the server ID is valid
  if (!serverId || !bot.guilds.cache.get(serverId)) {
    return msg.channel.send("Invalid server ID!");
  }

  // Load the server data from the file
  const serverData = JSON.parse(
    await fs.promises.readFile(`./saves/${serverId}.json`),
  );

  // Create a new server from the server data
  const server = await bot.createGuild({
    name: serverData.name,
    id: serverData.id,
  });

  // Add the server members to the new server
  for (const member of serverData.members) {
    await server.addMember(member.id);
  }

  // Create the server channels from the server data
  for (const channel of serverData.channels) {
    await server.createChannel({
      name: channel.name,
      type: channel.type,
    });
  }

  // Set the server roles from the server data
  for (const role of serverData.roles) {
    await server.createRole({
      name: role.name,
      color: role.color,
    });
  }

  // Send a message to the user saying that the server has been loaded
  msg.channel.send("Server loaded successfully!");
  console.log("sever loaded");
}

// Register the commands
bot.registerCommand("save", saveServer, {
  requirements: {
    permissions: {
      administrator: true,
    },
  },
});

bot.registerCommand("load", loadServer, {
  requirements: {
    permissions: {
      administrator: true,
    },
  },
});

// Role All Command
bot.registerCommand(
  "role_all",
  (msg, args) => {
    if (msg.member.permission.has("manageRoles")) {
      const roleName = args.join(" ");
      const role = msg.channel.guild.roles.find((r) => r.name === roleName);
      if (role) {
        msg.channel.guild.members.forEach((member) => {
          member.addRole(role.id);
        });
        return (
          `All users have been assigned the role ${roleName}.`,
          colsole.log(`users have been assigned ${rolename}`)
        );
      } else {
        return `Role ${roleName} not found.`;
      }
    } else {
      return "You do not have permission to use this command.";
    }
  },
  {
    requirements: {
      permissions: {
        manageRoles: true,
      },
    },
  },
);

bot.on("messageCreate", async (message) => {
  if (message.content.startsWith("pls")) {
    console.log("user is a begger");
    bot.createMessage(message.channel.id, "your a begger");
  }
});

bot.registerCommand("skid", (msg, args) => {
  const [offset, hex, obf] = args;
  if (!offset || !hex) {
    return "Please provide a valid offset and hex value.";
  }

  const fileName = `Scripts/${msg.author.username}.lua`;
  const fileContent = `
     function Patch(offset, hex) 
     local lib = "libil2cpp.so
     local ms = ""
     if tabl0001 == nil then
     tabl0001 = {}
     end
     local targetAddr = 0
     local hexStrCount = hex:gsub("%s+", "") -- Remove spaces between hex
     if hexStrCount:len() % 2 ~= 0 then
     return print("Check your hex again. Something is wrong there.")
     end
     local hexCount = hexStrCount:len() / 2
     for i, v in ipairs(gg.getRangesList(lib)) do
     if v.type:sub(3, 3) == "x" then
     targetAddr = v.start + offset
     break
     end
     end
     local editHex = {}
     local ed = {}
     for i = 1, hexCount do
     editHex[i] = { address = targetAddr + (i - 1), flags = gg.TYPE_BYTE }
     end
     gg.loadResults(editHex)
     local res = gg.getResults(gg.getResultsCount())
     for i in ipairs(res) do
     ms = string.format("%x", res[i].value)
     ms = string.upper(ms)
     ms = ms:gsub("FFFFFFFFFFFFFF", "")
     if ms == "0" then
     ms = ms:gsub("0", "00")
     end
     if ms:len() == 1 then
     ms = "0" .. ms
     end
     ed[i] = ms
     end
     ms = table.concat(ed)
     ms = "h" .. ms
     local lob = #tabl0001 + 1
     local oft = #tabl0001 + 2
     local eiz = #tabl0001 + 3
     tabl0001[lob] = lib
     tabl0001[oft] = offset
     tabl0001[eiz] = ms
     gg.loadResults(editHex)
     gg.getResults(hexCount)
     gg.editAll("h" .. hex, gg.TYPE_BYTE)
     gg.clearResults()
     end
            function Restore(offset)
             for i = 1, #tabl0001 do
              if tabl0001[i] == lib and tabl0001[i + 1] == offset then
               edi = tabl0001[i + 2]
               hex = #tabl0001[i + 2] - 1
              end
             end
             for i, v in ipairs(gg.getRangesList()) do
              if v.type:sub(3, 3) == "x" then targetAddr = v.start + offset break end
             end
             local editHex = {}
             local ed = {}
             hex = hex / 2
             for i = 1, hex do
              editHex[i] = { address = targetAddr + (i - 1), flags = gg.TYPE_BYTE }
             end
             gg.loadResults(editHex)
             gg.getResults(gg.getResultsCount())
             gg.editAll(edi, 1)
             gg.clearResults()
            end
     off = "[off]" on = "[on]"
     co = off
     function yyy()
     cost= gg.choice({
     co.."patch",
     "back"
     },nil)
     if cost == 1 then
     if co == off then

     Patch(${offset}, "${hex}")

     co=on
     else
     Restore(${offset})
     co=off
     end
     yyy()
     end
     if cost == 2 or nil then
     os.exit()
     end
     end
     while(true) do 
     yyy()
     end

     `;

  if (obf === "yes") {
    (async () => {
      const code = `function Patch(v0,v1)local v2=\"libil2cpp.so\";local v3=\"\";if (tabl0001==nil) then tabl0001={};end local v4=0;local v5=v1:gsub(\"%s+\",\"\");if ((v5:len()%2)~=0) then return print(\"Check your hex again. Something is wrong there.\");end local v6=v5:len()/2 ;for v19,v20 in ipairs(gg.getRangesList(v2)) do if (v20.type:sub(3,3)==\"x\") then v4=v20.start + v0 ;break;end end local v7={};local v8={};for v21=1,v6 do v7[v21]={address=v4 + (v21-1) ,flags=gg.TYPE_BYTE};end gg.loadResults(v7);local v9=gg.getResults(gg.getResultsCount());for v23 in ipairs(v9) do v3=string.format(\"%x\",v9[v23].value);v3=string.upper(v3);v3=v3:gsub(\"FFFFFFFFFFFFFF\",\"\");if (v3==\"0\") then v3=v3:gsub(\"0\",\"00\");end if (v3:len()==1) then v3=\"0\"   .. v3 ;end v8[v23]=v3;end v3=table.concat(v8);v3=\"h\"   .. v3 ;local v10= #tabl0001 + 1 ;local v11= #tabl0001 + 2 ;local v12= #tabl0001 + 3 ;tabl0001[v10]=v2;tabl0001[v11]=v0;tabl0001[v12]=v3;gg.loadResults(v7);gg.getResults(v6);gg.editAll(\"h\"   .. v1 ,gg.TYPE_BYTE);gg.clearResults();end function Restore(v16)for v25=1, #tabl0001 do if ((tabl0001[v25]==lib) and (tabl0001[v25 + 1 ]==v16)) then edi=tabl0001[v25 + 2 ];hex= #tabl0001[v25 + 2 ] -1 ;end end for v26,v27 in ipairs(gg.getRangesList()) do if (v27.type:sub(3,3)==\"x\") then targetAddr=v27.start + v16 ;break;end end local v17={};local v18={};hex=hex/2 ;for v28=1,hex do v17[v28]={address=targetAddr + (v28-1) ,flags=gg.TYPE_BYTE};end gg.loadResults(v17);gg.getResults(gg.getResultsCount());gg.editAll(edi,1);gg.clearResults();end off=\"[off]\";on=\"[on]\";co=off;function yyy()cost=gg.choice({co   .. \"patch\" ,\"back\"},nil);if (cost==1) then if (co==off) then Patch(${offset},\"${hex}\");co=on;else Restore(${offset});co=off;end yyy();end if ((cost==2) or nil) then os.exit();end end while true do yyy();end`;
      const obfuscatedCode = await obfuscator(code);
      const fileName = `Scripts/${msg.author.username}.lua`;
      fs.writeFile(fileName, obfuscatedCode, (err) => {
        if (err) throw err;
        const channel = msg.channel.id;
        bot.createMessage(channel, "heres our script:", {
          file: fs.readFileSync(fileName), //{name: fileName}
          name: fileName,
        });
        console.log(`${msg.author.username} skidded a script`);
        //console.log(obfuscatedCode);
      });
    })();
  } else {
    const herx = `
 --[[
 
    ██╗  ██╗███████╗██████╗ ███╗██╗  ██╗███╗
    ██║  ██║██╔════╝██╔══██╗██╔╝╚██╗██╔╝╚██║
    ███████║█████╗  ██████╔╝██║  ╚███╔╝  ██║
    ██╔══██║██╔══╝  ██╔══██╗██║  ██╔██╗  ██║
    ██║  ██║███████╗██║  ██║███╗██╔╝ ██╗███║
    ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚══╝╚═╝  ╚═╝╚══╝

 ]]--
  `;
    fs.writeFile(fileName, herx + fileContent, (err) => {
      if (err) throw err;
      const channel = msg.channel.id;
      bot.createMessage(channel, "heres our script:", {
        //bot.sendMessage({
        file: fs.readFileSync(fileName), //{name: fileName}
        name: fileName,
      });
      console.log("user skidded a script");
      fs.unlink(fileName, (err) => {
        if (err) {
          throw err;
          colsole.log("error");
        }
      });
    });
  }
});
//});

bot.on("messageCreate", async (msg) => {
  const channel = msg.channel.id;
  const serv = "1154034131232116797";
  if (channel != serv) {
    return;
  } else {
    console.log();
  }
});

// Create a poll command
bot.registerCommand("poll", (msg) => {
  //Get the poll title and options
  const pollTitle = msg.content.split(" ")[1];
  const pollOptions = msg.content.split(" ").slice(2);
  const channel = msg.channel.id;
  const user = msg.author.username;
  // Create a new embed message for the poll
  //for (const option of pollOptions) {
  for (const option of pollOptions) {
    const em = {
      title: pollTitle,
      feilds: [
        {
          name: option,
          value: msg.id,
          option,
        }, //,
        // {
        //  name: option,
        //  value: msg.id, options
        // }
      ],

      footer: {
        text: `poll made by ${user}`,
      },
    };
    // Add the poll options to the embed
  }
  const embed = em;

  // Send the poll embed message
  bot.createMessage(channel, { embeds: [embed] });

  // Add reactions to the poll embed message for each option
  for (const option of pollOptions) {
    msg.addReaction(msg.id, option);
    console.log("user created a poll");
  }
});

// Command to display all available commands
bot.registerCommand(
  "helpme",
  (msg) => {
    const commandsEmbed = {
      title: "Available Commands",
      description: "Here is a list of available commands:",
      color: 0x3498db, // A nice blue color
      fields: [
        { name: `${commandPrefix}proplam`, value: "Lol, just use it." },
        {
          name: `${commandPrefix}helpme`,
          value: "Display available commands.",
        },
        { name: `${commandPrefix}ping`, value: "Responds with Pong!" },
        { name: `${commandPrefix}hello`, value: "Responds with Hello!" },
        { name: `${commandPrefix}userinfo`, value: "Provides user's info." },
        {
          name: `${commandPrefix}serverinfo`,
          value: "Provides server's info.",
        },
        {
          name: `${commandPrefix}channelinfo <channelId>`,
          value: "provides info about the specified channel",
        },
        { name: `${commandPrefix}dice`, value: "Rolls a six-sided die." },
        {
          name: `${commandPrefix}say <message>`,
          value: "Makes the bot say the provided message.",
        },
        {
          name: `${commandPrefix}invite`,
          value: "Sends an invite link for the bot.",
        },
        {
          name: `${commandPrefix}clear <amount>`,
          value:
            "Clears the specified number of messages (requires permissions).",
        },
        {
          name: `${commandPrefix}kick <user>`,
          value: "Kicks the specified user (requires permissions).",
        },
        {
          name: `${commandPrefix}role_all <role_name>`,
          value:
            "Assigns the specified role to all users in the server (requires permissions).",
        },
        {
          name: `${commandPrefix}role_user <user_mention> <role_name>`,
          value:
            "Assigns the specified role to a specific user (requires permissions).",
        },
        {
          name: `${commandPrefix}echo <channel> <message>`,
          value: "Echos a message (only for me).",
        },
        {
          name: `${commandPrefix}flip`,
          value: "Flips a coin (Heads or Tails).",
        },
        {
          name: `${commandPrefix}ball <question>`,
          value: "Ask the Magic 8-Ball a question.",
        },
        { name: `${commandPrefix}cat`, value: "Shows a random cat picture." },
        { name: `${commandPrefix}dog`, value: "Shows a random dog picture." },
        {
          name: `${commandPrefix}av <mention>`,
          value: "Shows the avatar of the mentioned user.",
        },
        {
          name: `${commandPrefix}stats <mention(optional)>`,
          value: "Shows the stats of the mentioned user.",
        },
        {
          name: `${commandPrefix}setlevel <level> <mention(optional)>`,
          value: "Sets level(requires permissions).",
        },
        {
          name: `${commandPrefix}setp <xp> <mention(optional)>`,
          value: "Sets xp(requires permissions).",
        },
      ],
      author: {
        name: "proplamistic bot",
        icon_url:
          "https://cdn.discordapp.com/attachments/1154034131232116797/1166113548087345252/394da376c186251053de01ced93536e0-removebg.png?ex=65494edf&is=6536d9df&hm=a83b8171f9c9609b7398fc073b15c8c1a92a28fa0305fbfef5f489f8dc59b8b8&",
      },
      //footer: { text: "Bot Commands | proplam" },
    };
    const commands2em = {
      //title: "Available Commands",
      //description: "Here is a list of available commands:",
      color: 0x3498db, // A nice blue color
      fields: [
        { name: `${commandPrefix}setprefix <prefix>`, value: "Sets prefix" },
        {
          name: `${commandPrefix}skid <offset> <hex> <obf yes/no>`,
          value: "Creates a script (for skids).",
        },
        {
          name: `${commandPrefix}obf <amogus(else feet)> <lua file>`,
          value: "feet/amongus obfuscates a lua script",
        },
        {
          name: `${commandPrefix}btf <lua file>`,
          value: "Beautifies Lua code.",
        },
        {
          name: `${commandPrefix}mnf <lua file>`,
          value: "minifies lua code",
        },
        {
          name: `${commandPrefix}runjs <code>`,
          value: "runs js code(only i can use this)",
        },
        {
          name: `${commandPrefix}runpy <code>`,
          value: "runs python code(only i can use this)",
        },
        {
          name: `${commandPrefix}runbash <code>`,
          value: "runs bash code(only i can use this)",
        },
        {
          name: `${commandPrefix}savecode <name> <code>`,
          value: "saves the code snippet",
        },
        {
          name: `${commandPrefix}loadcode <name>`,
          value: "loads the code snippet",
        },
        {
          name: `${commandPrefix}codestats <code>`,
          value: "gives stats about the code",
        },
        {
          name: `${commandPrefix}syntax <code>`,
          value: "checks errors",
        },
        {
          name: `${commandPrefix}cs <search>`,
          value: "searches through gist api",
        },
        // { name: `${commandPrefix}help`, value: "Displays this message" },
      ],
      footer: { text: "Bot Commands | proplam" },
      /*
      thumbnail: {
     url: "Thumbnail Image URL",
   },
   image: {
     url: "Large Image URL",
   },
   */
      timestamp: new Date(),
    };

    bot.createMessage(msg.channel.id, { embeds: [commandsEmbed] });

    return { embed: commands2em };
  },
  {
    description: "Display available commands.",
    fullDescription: "Display the list of available commands.",
  },
);

bot.on("messageCreate", async (msg) => {
  if (msg.content.startsWith(`${PREFIX}` + "obf")) {
    // Assuming the user sent a file as an attachment
    const attachment = msg.attachments[0];
    //const response = await readTextFromFile(attachment.url);
    let response = await fetch(attachment.url);
    response = await response.text(); // Read the response as text

    let fileobf = `obf_${msg.author.username}.lua`;
    let ocount = 0;
    while (ocount <= 10) {
      fileobf = fileobf.replace(" ", "-");
      ocount = ocount + 1;
    }
    const filede = `src/deobf_${msg.author.id}.lua`;
    fs.createWriteStream(filede);
    fs.createWriteStream("public/" + fileobf);
    fs.writeFileSync(filede, response, async (err) => {});
    //msg.channel.createMessage(`Text from the file: ${response}`);
    async function apiobf(msg, config) {
      if (config.includes("amogus")) {
        exec(
          `curl -X POST https://luaobfuscator.com/api/obfuscator/newscript \\
     -H "Content-type: multipart/form-data" \\
     -H "apikey: test" \\
     -F file.=@${filede} \\
     | curl -X POST https://luaobfuscator.com/api/obfuscator/obfuscate \\
     -H "Content-type: application/json" \\
     -H "apikey: test" \\
     --data "@config/amogus.json" \\
     -H "sessionId: $(grep -o -P \'(?<="sessionId":").*(?="})')" \\
     | jq -r ".code" \\
     > public/${fileobf}`,
          (err, stdout, stderr) => {
            if (err) {
              console.error(err);
              return;
            } else {
              const channel = msg.channel.id;
              let ob = fs.readFileSync("public/" + fileobf, "utf8");
              let lb = 0;
              while (lb <= 500) {
                ob = ob.replace("===", "---");
                ob = ob.replace("[======", "[------");
                ob = ob.replace("===]", "---]");
                ob = ob.replace("==]", "--]");
                ob = ob.replace("==-", "---");
                lb = lb + 1;
              }
              fs.writeFileSync("public/" + fileobf, ob);
              bot.createMessage(channel, "heres your amongus obf obf script:", {
                //bot.sendMessage({
                file: fs.readFileSync("public/" + fileobf), //{name: fileName}
                name: fileobf,
              });
              console.log("user amogus obfuscated script");
              fs.unlink("public/" + fileobf, (err) => {
                if (err) {
                  throw err;
                  colsole.log("error");
                }
              });
            }
          },
        );
      } else {
        exec(
          `curl -X POST https://luaobfuscator.com/api/obfuscator/newscript \\
      -H "Content-type: multipart/form-data" \\
      -H "apikey: test" \\
      -F file.lua=@${filede} \\
      | curl -X POST https://luaobfuscator.com/api/obfuscator/obfuscate \\
      -H "Content-type: application/json" \\
      -H "apikey: test" \\
      --data "@config/feet.json" \\
      -H "sessionId: $(grep -o -P \'(?<="sessionId":").*(?="})')" \\
      | jq -r ".code" \\
      > public/${fileobf}`,
          (err, stdout, stderr) => {
            if (err) {
              console.error(err);
              return;
            } else {
              const channel = msg.channel.id;
              let ob = fs.readFileSync("public/" + fileobf, "utf8");
              let lb = 0;
              while (lb <= 500) {
                ob = ob.replace("===", "---");
                ob = ob.replace("[======", "[------");
                ob = ob.replace("===]", "---]");
                ob = ob.replace("==]", "--]");
                ob = ob.replace("==-", "---");
                ob = ob.replace("=---", "----");
                lb = lb + 1;
              }
              fs.writeFileSync("public/" + fileobf, ob);
              bot.createMessage(channel, "heres your feet obf script:", {
                //bot.sendMessage({
                file: fs.readFileSync("public/" + fileobf), //{name: fileName}
                name: fileobf,
              });
              console.log("user feet obfuscated a script");
              fs.unlink("public/" + fileobf, (err) => {
                if (err) {
                  throw err;
                  colsole.log("error");
                }
              });
            }
          },
        );
      }
    }
    try {
      const compy = PREFIX + "obf";
      const config = msg.content.slice(compy.length).trim();
      apiobf(msg, config);
    } catch (error) {
      msg.channel.createMessage("An error occurred while reading the file.");
      console.error(error);
    }
  }
});

async function processFileContents(fileContents) {
  let proc = 1;
  while (proc <= 10) {
    fileContents.replace(/ /g, "");
    proc = proc + 1;
  }
  const filespace = fileContents.replace(/\n/g, ";");
  return filespace;
}

async function readTextFromFile(fileUrl) {
  const response = await fetch(fileUrl);
  if (response.status !== 200) {
    throw new Error("Failed to fetch the file.");
  }
  const fileBuffer = await response.arrayBuffer();
  const fileText = fileBuffer.toString("utf8");
  return fileText;
}

bot.on("messageCreate", async (msg) => {
  if (msg.content === `${PREFIX}` + "btf" && msg.attachments.length > 0) {
    const attachment = msg.attachments[0];

    // Check if the attached file is a Lua file
    if (attachment.filename.endsWith(".lua")) {
      const response = await fetch(attachment.url);
      if (response.ok) {
        const luaCode = await response.text(); // Read the response as text

        // Beautify the Lua code
        const beautifiedCode = beautifyLua(luaCode);

        // Save the beautified code to a temporary file
        const tempFileName = "beautified.lua";
        fs.writeFileSync(tempFileName, beautifiedCode);

        // Send the beautified code as a reply
        const channel = msg.channel.id;
        msg.channel.createMessage("Here is the beautified Lua code:", {
          file: fs.readFileSync(tempFileName),
          name: tempFileName,
        });

        // Clean up the temporary file
        fs.unlinkSync(tempFileName);
      } else {
        bot.createMessage(msg.channel.id, "Failed to fetch the Lua file.");
      }
    } else if (
      msg.content === `${PREFIX}` + "btf" &&
      !msg.attachments.length > 0
    ) {
      bot.createMessage(
        msg.channel.id,
        "Please attach a Lua file with a .lua extension.",
      );
    }
  }
});

bot.on("messageCreate", async (msg) => {
  if (msg.content === `${PREFIX}` + "mnf" && msg.attachments.length > 0) {
    const attachment = msg.attachments[0];

    // Check if the attached file is a Lua file
    if (attachment.filename.endsWith(".lua")) {
      const response = await fetch(attachment.url);
      if (response.ok) {
        const luaCode = await response.text(); // Read the response as text

        // Beautify the Lua code
        const beautifiedCode = minifyLua(luaCode);

        // Save the beautified code to a temporary file
        const tempFileName = "minified.lua";
        fs.writeFileSync(tempFileName, beautifiedCode);

        // Send the beautified code as a reply
        const channel = msg.channel.id;
        msg.channel.createMessage("Here is the minified Lua code:", {
          file: fs.readFileSync(tempFileName),
          name: tempFileName,
        });

        // Clean up the temporary file
        fs.unlinkSync(tempFileName);
      } else {
        bot.createMessage(msg.channel.id, "Failed to fetch the Lua file.");
      }
    } else {
      bot.createMessage(
        msg.channel.id,
        "Please attach a Lua file with a .lua extension.",
      );
    }
  }
});

bot.on("messageCreate", (msg) => {
  if (
    msg.content.startsWith(`${PREFIX}` + "eval") &&
    msg.author.id === "690666917328977941"
  ) {
    try {
      // Extract the code to evaluate
      const code = msg.content.slice("=eval".length).trim();

      // Evaluate the code
      const result = eval(code);

      // Send the result as a reply
      bot.createMessage(msg.channel.id, "Result:\n```js\n" + result + "```");
    } catch (error) {
      // Handle any errors during evaluation
      bot.createMessage(msg.channel.id, "Error:\n```js\n" + error + "```");
    }
  }
});

// Example to display code from a GitHub repository
bot.registerCommand(
  "github",
  (msg, args) => {
    const [username, repo, file] = args;

    fetch(`https://raw.githubusercontent.com/${username}/${repo}/main/${file}`)
      .then((res) => res.text())
      .then((code) => {
        msg.channel.createMessage(`\`\`\`js\n${code}\`\`\``);
      })
      .catch((err) => {
        msg.channel.createMessage("Error fetching code from the repository.");
      });
  },
  {
    description: "Fetch code from a GitHub repository",
    fullDescription:
      "Fetches and displays code from a specified GitHub repository.",
    usage: "<username> <repository> <file>",
    aliases: ["git"],
  },
);

bot.on("messageCreate", async (msg) => {
  if (
    msg.content.startsWith(PREFIX + "runjs") &&
    msg.author.id === "690666917328977941"
  ) {
    const commandn = PREFIX + "runjs";
    const code = msg.content.slice(commandn.length).trim();
    const filename = `tempCode_${Date.now()}.js`;
    const codePath = `./${filename}`;

    try {
      // Writing the code to a temporary file
      await writeFile(codePath, code, "utf-8");

      // Execute the code in a separate Node.js process
      const execution = spawn("node", [codePath]);

      let output = "";

      execution.stdout.on("data", (data) => {
        output += data.toString();
      });

      execution.stderr.on("data", (data) => {
        output += data.toString();
      });

      execution.on("close", async (code) => {
        await bot.createMessage(msg.channel.id, "```js\n" + output + "```");
        await unlink(codePath);
      });
    } catch (error) {
      await bot.createMessage(msg.channel.id, "```js\n" + error + "```");
    }
  } else if (
    msg.content.startsWith(PREFIX + "runcode") &&
    msg.author.id !== "69066691732897794"
  ) {
    return;
  }
});

bot.on("messageCreate", async (msg) => {
  if (msg.content.startsWith(PREFIX + "runpy")) {
    const compy = PREFIX + "runpy";
    const code = msg.content.slice(compy.length).trim();

    const execution = spawn("python3", ["-c", code]);

    let output = "";

    execution.stdout.on("data", (data) => {
      output += data.toString();
    });

    execution.stderr.on("data", (data) => {
      output += data.toString();
    });

    execution.on("close", async (code) => {
      await bot.createMessage(msg.channel.id, "```python\n" + output + "```");
    });
  }
});

bot.on("messageCreate", async (msg) => {
  if (msg.content.startsWith(PREFIX + "runbash")) {
    const commandba = PREFIX + "runbash";
    const command = msg.content.slice(commandba.length).trim();

    const execution = spawn("bash", ["-c", command]);

    let output = "";

    execution.stdout.on("data", (data) => {
      output += data.toString();
    });

    execution.stderr.on("data", (data) => {
      output += data.toString();
    });

    execution.on("close", async (code) => {
      await bot.createMessage(msg.channel.id, "```bash\n" + output + "```");
    });
  }
});

const codeSnippets = new Map();

bot.on("messageCreate", async (msg) => {
  if (msg.content.startsWith(PREFIX + "savecode")) {
    const [_, snippetName, code] = msg.content.split(" ");
    codeSnippets.set(snippetName, code);
    await bot.createMessage(
      msg.channel.id,
      `Code snippet '${snippetName}' saved!`,
    );
  } else if (msg.content.startsWith(PREFIX + "getcode")) {
    const codesa = PREFIX + "getcode";
    const snippetName = msg.content.slice(codesa.length).trim();
    const snippet = codeSnippets.get(snippetName);
    if (snippet) {
      await bot.createMessage(
        msg.channel.id,
        `Code snippet '${snippetName}':\n\`\`\`\n${snippet}\n\`\`\``,
      );
    } else {
      await bot.createMessage(
        msg.channel.id,
        `Code snippet '${snippetName}' not found.`,
      );
    }
  } else if (msg.content.startsWith(PREFIX + "listcodes")) {
    const embed = {
      title: "Code Snippets",
      description: "List of available code snippets:",
      fields: [],
      color: 0x0099ff,
    };

    codeSnippets.forEach((code, snippetName) => {
      embed.fields.push({
        name: snippetName,
        value: "```" + code + "```",
      });
    });

    await bot.createMessage(msg.channel.id, { embed });
  } else if (msg.content.startsWith(PREFIX + "deleteall")) {
    codeSnippets.clear();
    await bot.createMessage(
      msg.channel.id,
      "All code snippets have been deleted.",
    );
  } else if (msg.content.startsWith(PREFIX + "deletesnippet")) {
    const codesa = PREFIX + "deletesnippet";
    const snippetName = msg.content.slice(codesa.length).trim();

    if (codeSnippets.has(snippetName)) {
      codeSnippets.delete(snippetName);
      await bot.createMessage(
        msg.channel.id,
        `Code snippet '${snippetName}' has been deleted.`,
      );
    } else {
      await bot.createMessage(
        msg.channel.id,
        `Code snippet '${snippetName}' not found.`,
      );
    }
  }
});

bot.on("messageCreate", async (msg) => {
  const commandst = PREFIX + "codestats";
  if (msg.content.startsWith(commandst)) {
    const code = msg.content.slice(commandst.length).trim();

    const lines = code.split("\n").length;
    const words = code.split(/\s+/).length;
    const characters = code.length;

    const stats = `Lines: ${lines}, Words: ${words}, Characters: ${characters}`;
    await bot.createMessage(msg.channel.id, "```js\n" + stats + "```");
  }
});

const GITHUB_API_BASE = "https://api.github.com/gists";

bot.on("messageCreate", async (msg) => {
  //console.log('test')
  if (msg.content.startsWith(PREFIX + "cs")) {
    const query = msg.content.slice(PREFIX + "cs".length).trim();
    try {
      bot.createMessage(msg.channel.id, "fetching response...");
      console.log("getting response...");
      const response = await axios.get(`${GITHUB_API_BASE}/public`, {
        params: {
          q: query,
        },
      });
      const gists = response.data;

      if (gists.length > 0) {
        let resultMessage = "Found code snippets:\n";
        const MAX_LENGTH = 2000;
        let currentMessage = "";

        for (const gist of gists) {
          const gistInfo = `**${gist.description}**: ${gist.html_url}\n`;
          if (currentMessage.length + gistInfo.length >= MAX_LENGTH) {
            await bot.createMessage(
              msg.channel.id,
              "```js\n" + currentMessage + "```",
            );
            currentMessage = gistInfo;
          } else {
            currentMessage += gistInfo;
          }
        }

        if (currentMessage.length > 0) {
          await bot.createMessage(
            msg.channel.id,
            "```js\n" + currentMessage + "```",
          );
        }
      } else {
        await bot.createMessage(msg.channel.id, "No code snippets found.");
      }
    } catch (error) {
      await bot.createMessage(
        msg.channel.id,
        "Error searching for code: \n```js\n" + error + "```",
      );
    }
  }
});

bot.on("messageCreate", async (msg) => {
  if (msg.content.startsWith(PREFIX + "syntax")) {
    const commandsy = PREFIX + "syntax";
    const code = msg.content.slice(commandsy.length).trim();

    try {
      parseScript(code); // Replace with the appropriate parser method based on the language used
      await bot.createMessage(msg.channel.id, "Code syntax is valid.");
    } catch (error) {
      await bot.createMessage(
        msg.channel.id,
        "Code syntax check failed: \n```js\n" + error + "```",
      );
    }
  }
});

// Function to send error message to a specific channel
const sendErrorMessage = async (errorMessage) => {
  try {
    await bot.createMessage(
      errorChannelID,
      `❌ Error occurred:\n\`\`\`js\n${errorMessage}\`\`\``,
    );
  } catch (error) {
    console.error("Error sending error message:", error);
  }
};

// Global error handling for different error types
process.on("uncaughtException", (error) => {
  const errorMessage = `Uncaught Exception: ${error.message}\nAt: ${
    error.stack.split("\n")[1]
  }`;
  sendErrorMessage(errorMessage);
  console.log(errorMessage);
});

process.on("unhandledRejection", (reason, promise) => {
  const errorMessage = `Unhandled Rejection at: ${promise}, reason: ${reason}`;
  sendErrorMessage(errorMessage);
  console.log(errorMessage);
});

process.on("warning", (warning) => {
  const errorMessage = `Warning: ${warning.name}\nAt: ${
    warning.stack.split("\n")[1]
  }`;
  sendErrorMessage(errorMessage);
  console.log(errorMessage);
});

process.on("multipleResolves", (type, promise, reason) => {
  const errorMessage = `Multiple Resolves: Type ${type}, Promise: ${promise}, Reason: ${reason}`;
  sendErrorMessage(errorMessage);
  console.log(errorMessage);
});

// Error handling for Axios
axios.interceptors.response.use(null, (error) => {
  const errorMessage = `Axios Error: ${error.message}\nAt: ${
    error.stack.split("\n")[1]
  }`;
  sendErrorMessage(errorMessage);
  console.log(errorMessage);
  return Promise.reject(error);
});

// Error handling for Node Fetch
const originalFetch = fetch;
globalThis.fetch = async (...args) => {
  try {
    const response = await originalFetch(...args);
    if (!response.ok) {
      const error = new Error(`Fetch Error: ${response.statusText}`);
      error.response = response;
      throw error;
    }
    return response;
  } catch (error) {
    const errorMessage = `Fetch Error: ${error.message}\nAt: ${
      error.stack.split("\n")[1]
    }`;
    sendErrorMessage(errorMessage);
    console.log(errorMessage);
    throw error;
  }
};

// Example error for testing
bot.on("messageCreate", async (msg) => {
  if (msg.content === PREFIX + "test") {
    try {
      // Force different types of errors for testing
      throw new Error("This is a test uncaught exception!");
    } catch (error) {
      const errorMessage = `Uncaught Exception: ${error.message}\nAt: ${
        error.stack.split("\n")[1]
      }`;
      sendErrorMessage(errorMessage);
    }

    try {
      Promise.reject(new Error("This is a test unhandled rejection!"));
    } catch (error) {
      const errorMessage = `Unhandled Rejection: ${error.message}\nAt: ${
        error.stack.split("\n")[1]
      }`;
      sendErrorMessage(errorMessage);
    }

    process.emitWarning("This is a test warning!");

    process.emitWarning(
      "This is a test multiple resolve!",
      "MultipleResolvesWarning",
    );

    axios.get("https://invalid-url.com").catch((error) => {
      const errorMessage = `Axios Error: ${error.message}\nAt: ${
        error.stack.split("\n")[1]
      }`;
      sendErrorMessage(errorMessage);
    }); // Axios error

    fetch("https://invalid-url.com").catch((error) => {
      const errorMessage = `Fetch Error: ${error.message}\nAt: ${
        error.stack.split("\n")[1]
      }`;
      sendErrorMessage(errorMessage);
    }); // Node Fetch error
  }
});

console.log(
  `Enter ` +
    clc.red(`Ctrl + z`) +
    " to Exit.\n" +
    "Enter " +
    clc.blue("Ctrl + x") +
    " to Stop the bot\n" +
    "Enter " +
    clc.green("Ctrl + s") +
    " to Start the bot\n",
);

export { bot, eris }